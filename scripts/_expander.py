from typing import cast

import numpy as np
from pandas import DataFrame

from ._util import _clean


class Expander:

    def expand(self, table: DataFrame):
        output_rows = []
        for row in table.values:
            for value in cast(str, _clean(row[1])).split(";"):
                output_rows.append([row[0], value])
        return DataFrame(data=output_rows, columns=table.columns)
