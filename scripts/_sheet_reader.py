from pathlib import Path

import pandas as pd


class SheetReadWriter:

    def __init__(self, path: Path, sheetname: str | None = None) -> None:
        self._excel_source_path = path
        self._path = path
        self._sheetname = sheetname

    @property
    def as_data_frame(self) -> pd.DataFrame:
        return pd.read_excel(self._path, sheet_name=self._sheetname)

    def get_dataframe_from_sheet(self, sheet: str) -> pd.DataFrame:
        return pd.read_excel(self._path, sheet_name=sheet)

    def save_data_frame(self, data_frame: pd.DataFrame, path: Path | None):
        if path is None:
            path = self._excel_source_path.parent.joinpath("output.xlsx")
        data_frame.to_excel(path)
