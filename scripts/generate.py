# import Presentation class
# from pptx library
import argparse
from pathlib import Path
from typing import cast

from ._generator import Generator
from ._helper import load_as_template
from ._sheet_reader import SheetReadWriter

_path_str_pptx = "/mnt/c/Users/u092966/Documents/Output.pptx"
_path_str_excel = "/mnt/c/Users/u092966/Documents/temp.xlsx"

parser = argparse.ArgumentParser(
    description="Loads a given excel spreadsheet into a set of powerpoint slides"
)
parser.add_argument("input_file", type=Path, help="Input excel file to process")

parser.add_argument(
    "output_file",
    type=Path,
    help="Path to store output powerpoint",
    default=None,
    nargs="?",
)

# Optional argument
parser.add_argument(
    "--template",
    type=Path,
    help="A existing powerpoint file to use as template for measurements",
)

parser.add_argument(
    "--sheet",
    type=str,
    help="A particular sheet name to use for input excel file",
)


class ARGS:
    input_file: Path
    output_file: Path | None
    template: Path | None
    sheet: str | None


def _validate(args: ARGS):
    input_file = args.input_file
    output_file = args.output_file
    template_file = args.template
    if not input_file.exists():
        raise Exception(f"The path for the input excel file does not exist: {input_file}")
    if input_file.suffix != ".xlsx":
        raise Exception(f"Expected input excel file of type '.xlsx' but got {input_file.suffix}")
    if output_file:
        if not output_file.parent.exists():
            raise Exception(
                f"The directory path for the output powerpoint file does not exist: {output_file.parent}"
            )
        if output_file.suffix != ".pptx":
            raise Exception(
                f"Expected output powerpoint file of type '.pptx' but got {input_file.suffix}"
            )
    else:
        args.output_file = input_file.parent.joinpath("output.pptx")
    if template_file:
        if template_file.suffix != ".pptx":
            raise Exception(
                f"Expected template powerpoint file of type '.pptx' but got {input_file.suffix}"
            )


def get_args() -> ARGS:
    args = cast(ARGS, parser.parse_args())
    _validate(args)
    return args


def main():
    args = get_args()
    template = load_as_template(args.template)
    generator = Generator(template)
    sheet_reader = SheetReadWriter(args.input_file, sheetname=args.sheet)

    generator.add_table(sheet_reader.as_data_frame)

    generator.save(args.output_file)

    print("done")
