from typing import Any


def _clean(input: Any) -> str:
    val = str(input)
    if val == "nan":
        val = ""
    return val
