from pathlib import Path
from typing import Iterable, NamedTuple, cast

from pandas import DataFrame
from pptx import Presentation
from pptx.shapes.graphfrm import GraphicFrame
from pptx.shapes.shapetree import SlideShapes
from pptx.slide import Slide
from pptx.table import Table, _Column
from pptx.util import Inches

from ._helper import Template
from ._util import _clean


class TableSpan(NamedTuple):
    start_index: int
    stop_index: int

    @property
    def span(self) -> int:
        return self.stop_index - self.start_index


class TableSplitter:
    text_lines_coeffecient = 0.025

    def __init__(self, table_data: DataFrame) -> None:
        self._table_data = table_data
        self._current_pos = 0
        self._length = table_data.shape[0]

    def _get_max_cell_count_for_row(self, cells: Iterable[str]) -> int:
        max = 0
        for cell in cells:
            cell = _clean(cell)
            if len(cell) > max:
                max = len(cell)
        return max

    @property
    def has_rows_left(self):
        return self._current_pos < self._length

    def get_row_iterator(self):
        while self.has_rows_left:
            yield self._table_data.values[self._current_pos]
            self._current_pos += 1

    def get_next_table_span_for_total_size_of(self, size: float):
        nr_of_text_lines = 0
        start_position = self._current_pos
        try:
            row_iterator = self.get_row_iterator()
            while nr_of_text_lines < size:
                row = next(row_iterator)
                max_cell_text_size = self._get_max_cell_count_for_row(row)
                nr_of_text_lines += max_cell_text_size * self.text_lines_coeffecient
        except StopIteration:
            pass
        if start_position == self._current_pos:
            raise Exception(
                f"Unable to find next span as first element is already exceeding {size} line size ({nr_of_text_lines})"
            )
        return TableSpan(start_position, self._current_pos)

    def get_table_span_iterator(self, size: float):
        while self.has_rows_left:
            yield self.get_next_table_span_for_total_size_of(size)


class TableDataTracker:

    def __init__(self, table_data: DataFrame, rows_from: int, rows_to: int) -> None:
        self._table_data = table_data
        self._rows_from = rows_from
        self._rows_to = rows_to
        self.length = rows_to - rows_from
        self._current_row_position = 0
        self._current_column_position = 0
        self.nr_of_columns = self._table_data.shape[1]

    @property
    def row_index(self):
        return self._rows_from + self._current_row_position

    @property
    def column_index(self):
        return self._current_column_position

    @property
    def current_value(self) -> str:
        return _clean(self._table_data.values[self.row_index, self.column_index])

    @property
    def is_first(self):
        if self._current_row_position == 0:
            return True

    @property
    def previous_value(self) -> str | None:
        if self.row_index - 1 >= 0:
            return _clean(self._table_data.values[self.row_index - 1, self.column_index])

    def current_left_cell_by(self, left_count: int) -> str:
        return _clean(self._table_data.values[self.row_index, self.column_index - left_count])

    def previous_left_cell_by(self, left_count: int) -> str:
        return _clean(self._table_data.values[self.row_index - 1, self.column_index - left_count])

    def get_data_iterator(self):
        while self._current_row_position < self.length:
            self._current_column_position = 0
            while self._current_column_position < self.nr_of_columns:
                yield self.current_value
                self._current_column_position += 1
            self._current_row_position += 1

    @property
    def is_equivalent_to_previous(self):
        if self.is_first:
            return False
        if self.current_value == self.previous_value:
            column_span = self.column_index
            count = 0
            while count < column_span:
                count += 1
                current = self.current_left_cell_by(count)
                previous = self.previous_left_cell_by(count)
                if current != previous:
                    return False
            return True
        return False

    @property
    def position(self) -> tuple[int, int]:
        return (self._current_row_position, self._current_column_position)

    @property
    def cell_position(self) -> tuple[int, int]:
        return (self._current_row_position + 1, self._current_column_position)

    @property
    def previous_cell_position(self) -> tuple[int, int]:
        return (self._current_row_position, self._current_column_position)

    def get_previous_cell_position_from_reference(
        self, reference_row_position: int, reference_column_position: int
    ):
        return (reference_row_position - 1, reference_column_position)


class CellWithPosition:

    def __init__(self, table_instance: Table, row_position: int, column_position: int) -> None:
        self._table_instance = table_instance
        self._row_position = row_position
        self._column_position = column_position
        self._cell = table_instance.cell(self._row_position, self._column_position)

    @property
    def text(self):
        self._cell.text

    @property
    def is_spanned(self):
        return self._cell.is_spanned

    @text.setter
    def text(self, text: str):
        self._cell.text = text

    @property
    def previous(self):
        return CellWithPosition(self._table_instance, self._row_position - 1, self._column_position)

    @property
    def is_merge_origin(self):
        return self._cell.is_merge_origin

    @property
    def is_not_merge_origin(self):
        return not self._cell.is_merge_origin

    def split(self):
        self._cell.split()

    def merge(self, other: "CellWithPosition"):
        self._cell.merge(other._cell)

    def set_font(self, font_family: str, font_size: int, bold=False, italic=False):
        self._cell.text_frame._set_font(font_family, font_size, bold, italic)

    def merge_with_previous(self):
        previous = self.previous
        if previous.is_spanned:
            while previous.is_not_merge_origin:
                previous = previous.previous
            previous.split()
        previous.merge(self)


class Generator:

    font_size = 8
    maximum_nr_of_lines_per_table = 30
    max_table_height = Inches(1)

    column_widths = [Inches(4)]
    font_family = "Calibri"

    def __init__(self, template: Template | None = None) -> None:
        self._ppt = Presentation()
        self._template = template
        self._section_name = ""

    def update_template(self, template: Template):
        self._template = template

    @property
    def _blank_slide_layout(self):
        return self._ppt.slide_layouts[6]

    @property
    def _title_and_subtitle(self):
        return self._ppt.slide_layouts[0]

    @property
    def _title_and_content(self):
        return self._ppt.slide_layouts[1]

    @property
    def _section_header(self):
        return self._ppt.slide_layouts[2]

    @property
    def _two_content(self):
        return self._ppt.slide_layouts[3]

    @property
    def _title(self):
        return self._ppt.slide_layouts[5]

    @property
    def _pic_with_caption(self):
        return self._ppt.slide_layouts[8]

    def add_slide_with_title(self, title: str) -> Slide:
        slide = cast(Slide, self._ppt.slides.add_slide(self._title))
        slide.shapes[0].text = title
        return slide

    def add_section_header(self, generic_text: str, specific_text: str):
        slide = cast(Slide, self._ppt.slides.add_slide(self._section_header))
        slide.shapes[0].text = specific_text
        slide.shapes[1].text = generic_text
        self._section_name = specific_text

    def add_slide_with_text(self, text: str):
        slide = self._add_blank_slide()
        txBox = slide.shapes.add_textbox(Inches(1), Inches(1), Inches(1), Inches(1))
        text_frame = txBox.text_frame
        text_frame.text = text

    def _add_blank_slide(self) -> Slide:
        return cast(Slide, self._ppt.slides.add_slide(self._blank_slide_layout))

    def _generate_new_table_instance_on_blank_slide(self, nr_of_rows, nr_of_columns) -> Table:
        slide = self._add_blank_slide()
        return self._add_table_to_slide(nr_of_rows, nr_of_columns, slide)

    def _generate_new_table_instance_on_slide_with_header(
        self, nr_of_rows, nr_of_columns, header: str
    ) -> Table:
        slide = self.add_slide_with_title(header)
        return self._add_table_to_slide(nr_of_rows, nr_of_columns, slide)

    def _add_table_to_slide(self, nr_of_rows, nr_of_columns, slide):
        shapes = cast(SlideShapes, slide.shapes)
        height = (
            self._template.table_frame.height
            if self._template.table_frame.height < self.max_table_height
            else self.max_table_height
        )
        table_frame = cast(
            GraphicFrame,
            shapes.add_table(
                nr_of_rows,
                nr_of_columns,
                left=self._template.table_frame.left,
                top=self._template.table_frame.top,
                width=self._template.table_frame.width,
                height=height,
            ),
        )
        table = cast(Table, table_frame.table)
        if self._template.columns:
            assert len(self._template.columns) == len(table.columns)
            for index, column in enumerate(self._template.columns):
                cast(_Column, table.columns[index]).width = column.width
        return table

    def _bound(self, value: int, bound: int):
        if value > bound:
            return bound
        return value

    def _add_header(self, table_instance: Table, table: DataFrame):
        for index, column_text in enumerate(table.columns):
            cell = table_instance.cell(0, index)
            cell.text = column_text
            cell.text_frame._set_font(self.font_family, self.font_size, bold=True, italic=False)

    def _add_table_contents(
        self, table_instance: Table, table_data: DataFrame, rows_from: int, rows_to: int
    ):
        tracker = TableDataTracker(table_data, rows_from, rows_to)
        for cell_text in tracker.get_data_iterator():
            cell = CellWithPosition(table_instance, *tracker.cell_position)
            if tracker.is_equivalent_to_previous:
                cell.merge_with_previous()
            else:
                cell.text = cell_text
                cell.set_font(self.font_family, self.font_size)

    def add_table(self, table: DataFrame):
        splitter = TableSplitter(table)
        for table_span in splitter.get_table_span_iterator(self.maximum_nr_of_lines_per_table):
            table_length = table_span.span + 1  # add one row for column header
            nr_of_columns = table.shape[1]
            if self._section_name:
                table_instance = self._generate_new_table_instance_on_slide_with_header(
                    table_length, nr_of_columns, self._section_name
                )
            else:
                table_instance = self._generate_new_table_instance_on_blank_slide(
                    table_length, nr_of_columns
                )
            # add the header
            self._add_header(table_instance, table)
            # add the contents
            # the last_row_position are multiples of the rows_per_slide step
            self._add_table_contents(
                table_instance, table, table_span.start_index, table_span.stop_index
            )

    def save(self, path: Path):
        self._ppt.save(path)
        print(f"Power point presentation save in {path}")
