# import Presentation class
# from pptx library
from pathlib import Path

from ._generator import Generator
from ._helper import load_as_template
from ._sheet_reader import SheetReadWriter

_path_str_pptx = "/mnt/c/Users/u092966/Documents/Output.pptx"
_path_str_excel = "/mnt/c/Users/u092966/Documents/temp.xlsx"


def _add_section(
    generator: Generator,
    template_path: Path,
    sheet_reader: SheetReadWriter,
    sheet: str,
    header_gen: str,
    header_spec: str,
):
    template = load_as_template(template_path)
    generator.update_template(template)
    table_data = sheet_reader.get_dataframe_from_sheet(sheet)
    generator.add_section_header(header_gen, header_spec)
    generator.add_table(table_data)


def main():

    base_path = Path("/mnt/c/Users/u092966/Documents/")
    input_path = base_path.joinpath("input.xlsx")

    generic_template_path = base_path.joinpath("Template.pptx")
    env_template_path = base_path.joinpath("Template_env.pptx")
    nf_template_path = base_path.joinpath("Template_nf.pptx")
    sheet_reader = SheetReadWriter(input_path)
    generator = Generator()
    _add_section(
        generator,
        generic_template_path,
        sheet_reader,
        "merge_base",
        "VCRM",
        "NLWS Requirements",
    )
    _add_section(
        generator,
        generic_template_path,
        sheet_reader,
        "merge_cyber",
        "VCRM",
        "IT Requirements",
    )
    _add_section(
        generator,
        env_template_path,
        sheet_reader,
        "merge_env",
        "VCRM",
        "Env Requirements",
    )
    _add_section(
        generator,
        nf_template_path,
        sheet_reader,
        "NFSafety",
        "VCRM",
        "Non Functional Safety Requirements",
    )
    _add_section(
        generator,
        nf_template_path,
        sheet_reader,
        "NFILS",
        "VCRM",
        "Non Functional ILS Requirements",
    )
    _add_section(
        generator,
        nf_template_path,
        sheet_reader,
        "NFSOW",
        "VCRM",
        "Non Functional SOW Requirements",
    )
    _add_section(
        generator,
        nf_template_path,
        sheet_reader,
        "NFEnvAs",
        "VCRM",
        "Non Functional Env Assurance Requirements",
    )

    generator.save(base_path.joinpath("vcrm.pptx"))

    print("done")


if __name__ == "__main__":
    main()
