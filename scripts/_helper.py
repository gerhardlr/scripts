from pathlib import Path
from typing import Dict, Iterable, List, cast

from pptx import Presentation
from pptx.shapes.graphfrm import GraphicFrame
from pptx.slide import Slide
from pptx.table import Table, _Column, _Row
from pptx.text.text import Font
from pptx.util import Emu, Inches


class ColumnTemplate:

    def __init__(self, width: Emu) -> None:
        self.width = width

    def __str__(self) -> str:
        return f"width (inches): {self.width.inches}"


class TableFrameTemplate:
    def __init__(self, height: Emu, left: Emu, top: Emu, width: Emu) -> None:
        self.height = height
        self.left = left
        self.top = top
        self.width = width

    def __str__(self) -> str:
        return (
            f"\theight (inches): {self.height.inches}\n"
            f"\twidth (inches): {self.width.inches}\n"
            f"\ttop (inches): {self.top.inches}\n"
            f"\tleft (inches): {self.left.inches}\n"
        )


class Template:

    def __init__(self) -> None:
        self.columns: List[ColumnTemplate] = []
        self.table_frame: TableFrameTemplate | None = None

    @property
    def columns_not_already_specified(self) -> bool:
        if len(self.columns) == 0:
            return True
        return False

    @property
    def table_frame_not_already_scpecified(self) -> bool:
        return self.table_frame is None

    def __str__(self) -> str:
        str_list = "\n\t".join(
            [f"col[{index}]:\n\t\t{col}" for index, col in enumerate(self.columns)]
        )
        return "\nTableFrame: \n" f"{self.table_frame}" "Columns: \n" f"\t{str_list}"


def _load_as_template_from_file(path: Path):
    presentation = Presentation(path)
    template = Template()
    slides = cast(Iterable[Slide], presentation.slides)
    for slide in slides:
        shapes = cast(Iterable[GraphicFrame], slide.shapes)
        for shape in shapes:
            if shape.has_table:
                if template.table_frame_not_already_scpecified:
                    table = cast(Table, shape.table)
                    template.table_frame = TableFrameTemplate(
                        height=shape.height,
                        left=shape.left,
                        top=shape.top,
                        width=shape.width,
                    )
                    columns = cast(Iterable[_Column], table.columns)
                    if template.columns_not_already_specified:
                        for column in columns:
                            template.columns.append(ColumnTemplate(width=column.width))
    return template


def print_shape_data(input: Path, output_file: Path):
    presentation = Presentation(input)
    slides = cast(Iterable[Slide], presentation.slides)
    output: list[str] = []
    for slide_index, slide in enumerate(slides):
        output.append(f"Slide{slide_index}:")
        shapes = cast(Iterable[GraphicFrame], slide.shapes)
        for shape in shapes:
            for row_index, row in enumerate(shape.table.rows):
                row = cast(_Row, row)
                output.append(f"\trow{row_index}.height:{row.height.inches}")
                for cell_index, cell in enumerate(row.cells):
                    output.append(
                        f"\t\tcell{cell_index}.height:{cell.margin_bottom.inches}"
                    )

    output_file.write_text("\n".join(output))


def _print_slide_specifications(path: Path):
    presentation = Presentation(path)
    slides_template: Dict[int, Template] = {}
    slides = cast(Iterable[Slide], presentation.slides)
    for slide_index, slide in enumerate(slides):
        slides_template[slide_index + 1] = Template()
        template = slides_template[slide_index + 1]
        shapes = cast(Iterable[GraphicFrame], slide.shapes)
        for shape in shapes:
            if shape.has_table:
                if template.table_frame_not_already_scpecified:
                    table = cast(Table, shape.table)
                    template.table_frame = TableFrameTemplate(
                        height=shape.height,
                        left=shape.left,
                        top=shape.top,
                        width=shape.width,
                    )
                    columns = cast(Iterable[_Column], table.columns)
                    if template.columns_not_already_specified:
                        for column in columns:
                            template.columns.append(ColumnTemplate(width=column.width))

    return slides_template


def print_slide_specifications(path: Path):
    slides_template = _print_slide_specifications(path)
    output: list[str] = []
    for slide_index, template in slides_template.items():
        output.append(f"Slide{slide_index}:{template}")
    print("\n".join(output))


def _get_default_template() -> Template:
    template = Template()
    template.table_frame = TableFrameTemplate(
        height=Inches(2.43),
        left=Inches(1.3),
        top=Inches(0.7864),
        width=Inches(7.35),
    )
    template.columns = []
    return template


def load_as_template(path: Path | None):
    if path:
        return _load_as_template_from_file(path)
    return _get_default_template()
