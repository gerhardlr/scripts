# import Presentation class
# from pptx library
import argparse
from pathlib import Path
from typing import cast

import pandas as pd

from ._expander import Expander
from ._sheet_reader import SheetReadWriter

_path_str_excel = "/mnt/c/Users/u092966/Documents/expand.xlsx"

parser = argparse.ArgumentParser(
    description="Conerts a one to many relelationship from cell value based to row based list"
)
parser.add_argument("input_file", type=Path, help="Input excel file to process")
parser.add_argument(
    "output_file",
    type=Path,
    help="Path to store output excel",
    default=None,
    nargs="?",
)

# Optional argument
parser.add_argument(
    "--sheet",
    type=str,
    help="A particular sheet name to use for input excel file",
)


class ARGS:
    input_file: Path
    output_file: Path | None
    sheet: str | None


def _validate(args: ARGS):
    input_file = args.input_file
    output_file = args.output_file
    if not input_file.exists():
        raise Exception(
            f"The path for the input excel file does not exist: {input_file}"
        )
    if input_file.suffix != ".xlsx":
        raise Exception(
            f"Expected input excel file of type '.xlsx' but got {input_file.suffix}"
        )
    if output_file:
        if not output_file.parent.exists():
            raise Exception(
                f"The directory path for the output excel file does not exist: {output_file.parent}"
            )
        if output_file.suffix != ".xlsx":
            raise Exception(
                f"Expected output excel file of type '.xlsx' but got {input_file.suffix}"
            )
    else:
        args.output_file = input_file.parent.joinpath("output.xlsx")


def get_args() -> ARGS:
    args = cast(ARGS, parser.parse_args())
    _validate(args)
    return args


def main():
    args = get_args()
    sheet_read_writer = SheetReadWriter(args.input_file, args.sheet)
    expander = Expander()
    output = expander.expand(sheet_read_writer.as_data_frame)
    sheet_read_writer.save_data_frame(output, args.output_file)
    print("done")
