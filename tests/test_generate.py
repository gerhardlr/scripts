from pathlib import Path
from unittest.mock import Mock, patch

import pytest

from scripts._generator import TableSplitter
from scripts._helper import (
    load_as_template,
    print_shape_data,
    print_slide_specifications,
)
from scripts._sheet_reader import SheetReadWriter
from scripts.generate import ARGS, main


class CustomPaths:
    pptx = "/mnt/c/Users/u092966/Documents/Output.pptx"
    template = "/mnt/c/Users/u092966/Documents/Template.pptx"
    excel = "/mnt/c/Users/u092966/Documents/temp.xlsx"


@pytest.fixture(name="custom_paths_str")
def fxt_custom_paths_str():
    return CustomPaths()


@pytest.fixture(name="custom_args")
def fxt_custom_args(custom_paths_str: CustomPaths):
    with patch("scripts.generate.get_args") as get_args_mock:
        args = ARGS()
        args.input_file = Path(custom_paths_str.excel)
        args.output_file = Path(custom_paths_str.pptx)
        args.template = Path(custom_paths_str.template)
        get_args_mock.return_value = args
        yield


def test_it(custom_args):
    main()


def test_print_slide_specifications(custom_paths_str: CustomPaths):
    print_slide_specifications(Path(custom_paths_str.pptx))


def test_print_shape_elements(custom_paths_str: CustomPaths):
    print_shape_data(Path(custom_paths_str.pptx), Path(".build/temp.txt"))


def test_helper(custom_paths_str: CustomPaths):
    template = load_as_template(Path(custom_paths_str.pptx))
    print(template)


def test_table_splitter(custom_paths_str: CustomPaths):
    reader = SheetReadWriter(Path(custom_paths_str.excel))
    splitter = TableSplitter(reader.as_data_frame)
    for table_span in splitter.get_table_span_iterator(30):
        print(table_span)


@pytest.fixture(name="adapt_args_for_merge_test")
def fxt_custom_args_for_merge_test(custom_paths_str: CustomPaths):
    custom_paths_str.excel = "/mnt/c/Users/u092966/Documents/temp2.xlsx"


def test_merge(adapt_args_for_merge_test, custom_args):
    main()
