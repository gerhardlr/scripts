from pathlib import Path
from unittest.mock import patch

import pytest

from scripts.expand import ARGS, main


class CustomPaths:
    excel_out = "/mnt/c/Users/u092966/Documents/expanded.xlsx"
    excel_in = "/mnt/c/Users/u092966/Documents/input.xlsx"
    excel_sheet = None


@pytest.fixture(name="custom_paths_str")
def fxt_custom_paths_str():
    return CustomPaths()


@pytest.fixture(name="custom_args")
def fxt_custom_args(custom_paths_str: CustomPaths):
    with patch("scripts.expand.get_args") as get_args_mock:
        args = ARGS()
        args.input_file = Path(custom_paths_str.excel_in)
        args.output_file = Path(custom_paths_str.excel_out)
        args.sheet = custom_paths_str.excel_sheet
        get_args_mock.return_value = args
        yield


@pytest.fixture(name="custom_paths_str2")
def fxt_custom_paths_str2(custom_paths_str: CustomPaths):
    custom_paths_str.excel_sheet = "Base"


@pytest.mark.usefixtures("custom_args")
def test_it():
    main()


@pytest.mark.usefixtures("custom_paths_str2", "custom_args")
def test_it_2():
    main()
